<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

require_once  FCPATH . 'vendor/autoload.php';    

class EnviosMasivos  {
    
    public function __construct() {
  //  parent::__construct();
        
    }
    
    /**
     * @return void Chequea si hay correos sin enviar y si hay partidos para mandar correo masivo.
     */
    public function cron(){
        /** Chequeo si hay correos sin enviar **/
            /** si hay correos sin enviar **/
                $this->envioCorreo($from,$to,$subject,$text);
        
        /** Chequeo partidos **/
                $this->checkPartidos();
                
                /** Si da true **/
                    $this->envioMasivoCorreo($idpartido);
                    
    }
    
    /**
    * @param string $from
    * @param string $to
    * @param string $subject
    * @param string $text
    * @return boolean envio de correo
    */
    
    public function enviarCorreo($from,$to,$subject,$text){
        $this->guardoCorreo($from,$to,$subject,$text);
    }
   /**
    * 
    * @param string $from
    * @param string $to
    * @param string $subject
    * @param string $text
    * @return boolean envio de correo
    */
    private function envioCorreo($from,$to,$subject,$text){
       
        $correo = new \Mailgun\Mailgun('d80cb3516aba38d56db3326863547120-b6183ad4-09b3c55d');
        
        $domain = 'sandboxf3ce4374954c4c3da8760bc31d339a89.mailgun.org';
        try{
            
            $result = $correo->sendMessage($domain, array(
                'from'    => $from,
                'to'      => $to,
                'subject' => $subject,
                'text'    => $text,
            ));
            
        } catch (Exception $ex) {
            $this->guardoCorreo($from, $to, $subject, $text);
            return false;
        }
        return true;
    }
    
    /**
     * 
     * @param interger $idcorreo
     * @return void
     */
    public function marcoCorreoEnviado($idcorreo){
        
    }
    /**
     * @return boolean Chequea si existen partidos próximamente.
     */
    private function checkPartidos(){
        /** Consulto horario en la base de datos. **/
        
        
    }
    
    /**
     * @param integer $id id del partido que tenemos que anunciar.
     * @return void
     */
    private function envioMasivoCorreo($id){
        /** tomo todos los usuarios.  **/
        
        /** envío los correos de a 1. **/
            $this->envioCorreo($from,$to,$subject,$text);
            
        $this->guardoResultado($resultado);
    }
    
    /**
     * 
     * @param type $from
     * @param type $to
     * @param type $subject
     * @param type $text
     * @return boolean Envío de correo
     */
    private function guardoCorreo($from,$to,$subject,$text){
        /*
         * Guardo en base de datos un correo que no fue enviado para que el cron lo envìe. 
         */
    }
    
    /**
     * 
     * @param type $resultado
     */
    private function guardoResultado($resultado){
        /** Resultado a base de datos **/
    }
}